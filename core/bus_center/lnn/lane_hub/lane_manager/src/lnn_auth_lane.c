/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lnn_auth_lane.h"

#include <securec.h>

#include "auth_manager.h"
#include "bus_center_info_key.h"
#include "bus_center_manager.h"
#include "common_list.h"
#include "lnn_lane.h"
#include "lnn_lane_interface.h"
#include "lnn_log.h"
#include "lnn_trans_lane.h"
#include "softbus_adapter_mem.h"
#include "softbus_adapter_thread.h"
#include "softbus_common.h"
#include "softbus_errcode.h"

#define INVALID_LANEID (-1)

typedef struct {
    ListNode node;
    uint32_t laneReqId;
    uint64_t laneId;
    uint32_t authRequestId;
    int64_t authId;
    char networkId[NETWORK_ID_BUF_LEN];
    AuthConnCallback callback;
} AuthReqInfo;

static SoftBusList *g_authReqList;

int32_t DelAuthReqInfoByAuthId(int64_t authId)
{
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return SOFTBUS_ERR;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->authId == authId) {
            ListDelete(&item->node);
            SoftBusFree(item);
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    return SOFTBUS_OK;
}

void AuthFreeLane(int64_t authId)
{
    uint32_t laneReqId = 0;
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->authId == authId) {
            laneReqId = item->laneReqId;
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    
    GetLaneManager()->lnnFreeLane(laneReqId);
    LNN_LOGI(LNN_LANE, "auth free lane, laneReqId=%{public}u", laneReqId);
}

static void InitAuthLane(const ILaneIdStateListener *listener)
{
    (void)listener;
    if (g_authReqList == NULL) {
        g_authReqList = CreateSoftBusList();
        if (g_authReqList == NULL) {
            LNN_LOGE(LNN_LANE, "create g_authReqList fail");
            return;
        }
    }
    LNN_LOGI(LNN_LANE, "g_authReqList init success");
}

static void DeInitAuthLane(void)
{
    if (g_authReqList == NULL) {
        LNN_LOGE(LNN_LANE, "g_authReqList is NULL");
        return;
    }
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        ListDelete(&item->node);
        SoftBusFree(item);
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    DestroySoftBusList(g_authReqList);
    g_authReqList = NULL;
}

int32_t AddAuthReqNode(const char *networkId, uint32_t laneReqId, uint32_t authRequestId,
    AuthConnCallback *callback)
{
    if (networkId == NULL || laneReqId == INVALID_LANE_REQ_ID || callback == NULL) {
        LNN_LOGE(LNN_LANE, "param invalid");
        return SOFTBUS_INVALID_PARAM;
    }
    AuthReqInfo *newItem = (AuthReqInfo *)SoftBusMalloc(sizeof(AuthReqInfo));
    if (newItem == NULL) {
        LNN_LOGE(LNN_LANE, "AuthReqInfo malloc fail");
        return SOFTBUS_ERR;
    }
    if (memcpy_s(&newItem->callback, sizeof(AuthConnCallback), callback, sizeof(AuthConnCallback)) != EOK) {
        LNN_LOGE(LNN_LANE, "memcpy_s callback fail");
        SoftBusFree(newItem);
        return SOFTBUS_MEM_ERR;
    }
    if (memcpy_s(&newItem->networkId, NETWORK_ID_BUF_LEN, networkId, NETWORK_ID_BUF_LEN) != EOK) {
        LNN_LOGE(LNN_LANE, "memcpy_s networkId fail");
        SoftBusFree(newItem);
        return SOFTBUS_MEM_ERR;
    }
    newItem->laneReqId = laneReqId;
    newItem->authRequestId = authRequestId;
    ListInit(&newItem->node);

    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        SoftBusFree(newItem);
        return SOFTBUS_ERR;
    }
    ListTailInsert(&g_authReqList->list, &newItem->node);
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    return SOFTBUS_OK;
}

void AuthOnLaneRequestFail(uint32_t laneReqId, int32_t reason)
{
    LNN_LOGI(LNN_LANE, "request failed, laneReqId=%{public}u, reason=%{public}d", laneReqId, reason);
    AuthConnCallback cb;
    cb.onConnOpenFailed = NULL;
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->laneReqId == laneReqId) {
            cb.onConnOpenFailed = item->callback.onConnOpenFailed;
            ListDelete(&item->node);
            SoftBusFree(item);
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    if (cb.onConnOpenFailed != NULL) {
        cb.onConnOpenFailed(laneReqId, reason);
    }
}

void AuthOnLaneStateChange(uint32_t laneReqId, LaneState state)
{
    /* current no treatment */
    (void)laneReqId;
    (void)state;
}

static void OnAuthConnOpenedSucc(uint32_t authRequestId, AuthHandle authHandle)
{
    LNN_LOGI(LNN_LANE, "open auth success with authRequestId=%{public}u", authRequestId);
    AuthConnCallback cb;
    cb.onConnOpened = NULL;
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->authRequestId == authRequestId) {
            item->authId = authHandle.authId;
            cb.onConnOpened = item->callback.onConnOpened;
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    if (cb.onConnOpened != NULL) {
        cb.onConnOpened(authRequestId, authHandle);
    }
}

static void OnAuthConnOpenedFail(uint32_t authRequestId, int32_t reason)
{
    LNN_LOGI(LNN_LANE, "open auth fail with authRequestId=%{public}u", authRequestId);
    uint32_t laneReqId = 0;
    AuthConnCallback cb;
    cb.onConnOpenFailed = NULL;
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->authRequestId == authRequestId) {
            laneReqId = item->laneReqId;
            cb.onConnOpenFailed = item->callback.onConnOpenFailed;
            ListDelete(&item->node);
            SoftBusFree(item);
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    if (cb.onConnOpenFailed != NULL) {
        cb.onConnOpenFailed(authRequestId, reason);
    }
    GetLaneManager()->lnnFreeLane(laneReqId);
}

static void DelAuthRequestItem(uint32_t laneReqId)
{
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->laneReqId == laneReqId) {
            ListDelete(&item->node);
            SoftBusFree(item);
            break;
        }
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
}

void AuthOnLaneRequestSuccess(uint32_t laneReqId, const LaneConnInfo *laneConnInfo)
{
    LNN_LOGI(LNN_LANE, "request success, laneReqId=%{public}u", laneReqId);
    AuthReqInfo *item = NULL;
    AuthReqInfo *next = NULL;
    uint32_t authRequestId = 0;
    if (SoftBusMutexLock(&g_authReqList->lock) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get lock fail");
        return;
    }
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_authReqList->list, AuthReqInfo, node) {
        if (item->laneReqId == laneReqId) {
            authRequestId = item->authRequestId;
            item->laneId = INVALID_LANEID;
            break;
        }
    }
    char uuid[UUID_BUF_LEN] = {0};
    if (LnnGetRemoteStrInfo(item->networkId, STRING_KEY_UUID, uuid, sizeof(uuid)) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get peer uuid fail");
        (void)SoftBusMutexUnlock(&g_authReqList->lock);
        return;
    }
    (void)SoftBusMutexUnlock(&g_authReqList->lock);
    AuthConnInfo authConnInfo;
    if (memset_s(&authConnInfo, sizeof(AuthConnInfo), 0, sizeof(AuthConnInfo)) != EOK) {
        LNN_LOGE(LNN_LANE, "memset_s authConnInfo fail");
        return;
    }
    if (GetAuthConn(uuid, laneConnInfo->type, &authConnInfo) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "GetAuthConn fail");
        return;
    }

    AuthConnCallback cb = {
        .onConnOpened = OnAuthConnOpenedSucc,
        .onConnOpenFailed = OnAuthConnOpenedFail,
    };
    LNN_LOGI(LNN_LANE, "open auth with authRequestId=%{public}u", authRequestId);
    if (AuthOpenConn(&authConnInfo, authRequestId, &cb, false) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "open auth conn fail");
        DelAuthRequestItem(laneReqId);
    }
}

static LaneInterface g_authLaneObject = {
    .Init = InitAuthLane,
    .Deinit = DeInitAuthLane,
    .allocLaneByQos = AuthAlloc,
    .FreeLane = AuthFree,
};

LaneInterface *AuthLaneGetInstance(void)
{
    return &g_authLaneObject;
}

static int32_t ConvertAuthLinkToLaneLink(AuthLinkTypeList *authLinkType, LanePreferredLinkList *laneLinkType)
{
    if (authLinkType == NULL || laneLinkType == NULL) {
        LNN_LOGE(LNN_LANE, "param invalid");
        return SOFTBUS_INVALID_PARAM;
    }
    for (uint32_t i = 0; i < authLinkType->linkTypeNum; ++i) {
        switch (authLinkType->linkType[i]) {
            case AUTH_LINK_TYPE_WIFI:
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_WLAN_2P4G;
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_WLAN_5G;
                break;
            case AUTH_LINK_TYPE_BR:
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_BR;
                break;
            case AUTH_LINK_TYPE_BLE:
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_BLE;
                break;
            case AUTH_LINK_TYPE_P2P:
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_P2P;
                break;
            case AUTH_LINK_TYPE_ENHANCED_P2P:
                laneLinkType->linkType[laneLinkType->linkTypeNum++] = LANE_HML;
                break;
            default:
                break;
        }
    }
    return SOFTBUS_OK;
}

int32_t AuthGetRequestOption(const char *networkId, LaneRequestOption *option)
{
    if (networkId == NULL || option == NULL) {
        LNN_LOGE(LNN_LANE, "param invalid");
        return SOFTBUS_INVALID_PARAM;
    }
    if (memcpy_s(option->requestInfo.trans.networkId, NETWORK_ID_BUF_LEN, networkId, NETWORK_ID_BUF_LEN) != EOK) {
        LNN_LOGE(LNN_LANE, "networkId callback fail");
        return SOFTBUS_MEM_ERR;
    }

#define DEFAULT_PID 0
    option->type = LANE_TYPE_AUTH;
    option->requestInfo.trans.pid = DEFAULT_PID;
    option->requestInfo.trans.p2pOnly = false;
    option->requestInfo.trans.networkDelegate = false;
    option->requestInfo.trans.transType = LANE_T_BYTE;
    option->requestInfo.trans.expectedBw = 0;
    option->requestInfo.trans.acceptableProtocols = LNN_PROTOCOL_ALL ^ LNN_PROTOCOL_NIP;

    AuthLinkTypeList authList;
    if (memset_s(&authList, sizeof(AuthLinkTypeList), 0, sizeof(AuthLinkTypeList)) != EOK) {
        LNN_LOGE(LNN_LANE, "memset_s authList fail");
        return SOFTBUS_ERR;
    }
    if (GetAuthLinkTypeList(networkId, &authList) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "get authList fail");
        return SOFTBUS_ERR;
    }
    if (ConvertAuthLinkToLaneLink(&authList, &option->requestInfo.trans.expectedLink) != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "convert authLink to laneLink fail");
        return SOFTBUS_ERR;
    }
    return SOFTBUS_OK;
}