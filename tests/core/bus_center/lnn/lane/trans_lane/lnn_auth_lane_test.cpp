/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <thread>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <securec.h>

#include "lnn_auth_lane.h"
#include "lnn_auth_lane.c"
#include "lnn_lane_common.h"
#include "lnn_lane_deps_mock.h"
#include "lnn_log.h"
#include "lnn_trans_lane.h"

namespace OHOS {
using namespace testing::ext;
using namespace testing;

class LNNAuthLaneMockTest : public testing::Test {
public:
    static void SetUpTestCase();
    static void TearDownTestCase();
    void SetUp();
    void TearDown();
};

void LNNAuthLaneMockTest::SetUpTestCase()
{
    GTEST_LOG_(INFO) << "LNNAuthLaneMockTest start";
    LnnInitLaneLooper();
}

void LNNAuthLaneMockTest::TearDownTestCase()
{
    LnnDeinitLaneLooper();
    GTEST_LOG_(INFO) << "LNNAuthLaneMockTest end";
}

void LNNAuthLaneMockTest::SetUp()
{
}

void LNNAuthLaneMockTest::TearDown()
{
}

static void OnConnOpenedTest(uint32_t requestId, AuthHandle authHandle)
{
    LNN_LOGI(LNN_TEST, "OnConnOpenedTest: requestId=%{public}d, authId=%{public}" PRId64 ".",
        requestId, authHandle.authId);
}

static void OnConnOpenFailedTest(uint32_t requestId, int32_t reason)
{
    LNN_LOGI(LNN_TEST, "OnConnOpenFailedTest: requestId=%{public}d, reason=%{public}d.", requestId, reason);
}

/*
* @tc.name: LNN_AUTH_REQUEST_LIST_TEST_001
* @tc.desc: lnn auth request list test
* @tc.type: FUNC
* @tc.require:
*/
HWTEST_F(LNNAuthLaneMockTest, LNN_AUTH_REQUEST_LIST_TEST_001, TestSize.Level1)
{
    LaneInterface *authObj = AuthLaneGetInstance();
    EXPECT_TRUE(authObj != nullptr);
    authObj->Init(nullptr);

    const char *networkId = "testnetworkid123";
    uint32_t laneReqId = 1;
    uint32_t authRequestId = 1;
    AuthConnCallback callback = {
        .onConnOpened = OnConnOpenedTest,
        .onConnOpenFailed = OnConnOpenFailedTest,
    };
    int32_t ret = AddAuthReqNode(NULL, laneReqId, authRequestId, &callback);
    EXPECT_TRUE(ret == SOFTBUS_INVALID_PARAM);

    ret = AddAuthReqNode(networkId, INVALID_LANE_REQ_ID, authRequestId, &callback);
    EXPECT_TRUE(ret == SOFTBUS_INVALID_PARAM);

    ret = AddAuthReqNode(networkId, laneReqId, authRequestId, NULL);
    EXPECT_TRUE(ret == SOFTBUS_INVALID_PARAM);

    ret = AddAuthReqNode(networkId, laneReqId, authRequestId, &callback);
    EXPECT_TRUE(ret == SOFTBUS_OK);

    authObj->Deinit();
}

/*
* @tc.name: LNN_AUTH_REQUEST_LIST_TEST_002
* @tc.desc: lnn auth request list test
* @tc.type: FUNC
* @tc.require:
*/
HWTEST_F(LNNAuthLaneMockTest, LNN_AUTH_REQUEST_LIST_TEST_002, TestSize.Level1)
{
    LaneInterface *authObj = AuthLaneGetInstance();
    EXPECT_TRUE(authObj != nullptr);
    authObj->Init(nullptr);

    const char *networkId = "testnetworkid123";
    uint32_t laneReqId = 1;
    uint32_t authRequestId = 1;
    AuthConnCallback callback = {
        .onConnOpened = OnConnOpenedTest,
        .onConnOpenFailed = OnConnOpenFailedTest,
    };

    int32_t ret = AddAuthReqNode(networkId, laneReqId, authRequestId, &callback);
    EXPECT_TRUE(ret == SOFTBUS_OK);

    LaneConnInfo info;
    (void)memset_s(&info, sizeof(LaneConnInfo), 0, sizeof(LaneConnInfo));
    LaneDepsInterfaceMock mock;
    EXPECT_CALL(mock, LnnGetRemoteStrInfo).WillRepeatedly(Return(SOFTBUS_OK));
    AuthOnLaneRequestSuccess(laneReqId, &info);

    AuthHandle authHandle;
    (void)memset_s(&info, sizeof(LaneConnInfo), 0, sizeof(LaneConnInfo));
    authHandle.authId = 1;
    OnAuthConnOpenedSucc(authRequestId, authHandle);

    ret = DelAuthReqInfoByAuthId(authHandle.authId);
    EXPECT_TRUE(ret == SOFTBUS_OK);

    authObj->Deinit();
}

/*
* @tc.name: AUTH_GET_REQUEST_OPTION_TEST_001
* @tc.desc: auth get request option test test
* @tc.type: FUNC
* @tc.require:
*/
HWTEST_F(LNNAuthLaneMockTest, AUTH_GET_REQUEST_OPTION_TEST_001, TestSize.Level1)
{
    const char *networkId = "testnetworkid123";
    LaneRequestOption option;
    (void)memset_s(&option, sizeof(LaneRequestOption), 0, sizeof(LaneRequestOption));

    int32_t ret = AuthGetRequestOption(NULL, &option);
    EXPECT_TRUE(ret == SOFTBUS_INVALID_PARAM);

    ret = AuthGetRequestOption(networkId, NULL);
    EXPECT_TRUE(ret == SOFTBUS_INVALID_PARAM);

    ret = AuthGetRequestOption(networkId, &option);
    EXPECT_TRUE(ret == SOFTBUS_OK);
}
} // namespace OHOS